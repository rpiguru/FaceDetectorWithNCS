sudo apt install -y python3-gi python3-dev python3-gst-1.0 gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-tools
sudo pip3 install -r requirements.txt
cd ~/
wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-206.0.0-linux-x86_64.tar.gz
tar xf google-cloud-sdk-206.0.0-linux-x86_64.tar.gz
CLOUDSDK_CORE_DISABLE_PROMPTS=1 ./google-cloud-sdk/install.sh
source .bashrc
gcloud activate-service-account --key-file face-recognition-model/sa-account.json
gcloud config set project toutaudio
openssl genrsa -out rsa_private.pem 2048 && openssl req -x509 -nodes -newkey rsa:2048 -keyout rsa_private.pem     -days 1000000 -out rsa_cert.pem -subj "/CN=unused"
gcloud iot devices create s$(grep Serial /proc/cpuinfo | cut -c11-26) --project=toutaudio --region=us-central1 --registry=RBPi --public-key path=rsa_cert.pem,type=rs256
sudo cp face-recognition-model/face-recognition.service /etc/systemd/system/face-recognition.service
sudo systemctl daemon-reload
sudo systemctl enable face-recognition.service
sudo systemctl start face-recognition.service