import os
import cv2

path = os.path.join(os.path.dirname(__file__), 'ncs', 'haarcascade_frontalface_alt.xml')
cascade = cv2.CascadeClassifier(path)


def detect_face(frame):
    height, width, _ = frame.shape
    result_data = []
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    try:
        results = cascade.detectMultiScale(gray_frame, scaleFactor=1.1, minNeighbors=3, minSize=(100, 100),
                                           flags=cv2.CASCADE_SCALE_IMAGE)
    except:
        results = ()
        pass

    if results != ():
        for r in results:
            # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
            result_data.append({
                'rect': [r[0], r[1], r[0] + r[2], r[1] + r[3]],
                'frame': frame[r[1]:r[1] + r[3], r[0]:r[0] + r[2]],
            })

    # Sort frames with their sizes
    sorted_data = sorted(result_data, key=lambda f: (f['rect'][2] - f['rect'][0]) * (f['rect'][3] - f['rect'][1]),
                         reverse=True)
    return sorted_data


if __name__ == '__main__':
    import time

    DIMENSION_VIDEO = (640, 480)

    cap = cv2.VideoCapture(0)

    while True:
        ret, _frame = cap.read()
        if not ret:
            break
        s_time = time.time()
        _frame = cv2.resize(_frame, DIMENSION_VIDEO)
        _faces = detect_face(_frame)
        if _faces:
            print('Elapsed: {}'.format(time.time() - s_time))
        for _r in [f['rect'] for f in _faces]:
            cv2.rectangle(_frame, (_r[0], _r[1]), (_r[2], _r[3]), (0, 0, 255), 2)
        cv2.imshow("Faces", _frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
