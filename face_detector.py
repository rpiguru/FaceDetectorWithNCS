import os
import time
import numpy as np
import cv2


_cur_dir = os.path.dirname(os.path.realpath(__file__))


detect_net = cv2.dnn.readNetFromCaffe(os.path.join(_cur_dir, "caffe", "deploy.prototxt"),
                                      os.path.join(_cur_dir, "caffe", "res10_300x300_ssd_iter_140000.caffemodel"))


def detect_face(frame):
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))

    # pass the blob through the network and obtain the detections and
    # predictions
    detect_net.setInput(blob)
    detections = detect_net.forward()

    faces = []

    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < 0.5:
            continue

        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")

        # enlarge the bounding box by approximately 20%
        sx = max(0, int(1.2 * startX - 0.2 * endX))
        sy = max(0, int(1.2 * startY - 0.2 * endY))
        ex = min(w, int(1.2 * endX - 0.2 * startX))
        ey = min(h, int(1.2 * endY - 0.2 * startY))

        # slice a face from a frame
        face = frame[sy:ey, sx:ex]
        faces.append(dict(
            rect=[sx, sy, ex, ey],
            frame=face
        ))
    sorted_faces = sorted(faces, key=lambda f: abs(f['rect'][2] - f['rect'][0]) * abs(f['rect'][3] - f['rect'][1]),
                          reverse=True)
    return sorted_faces


if __name__ == '__main__':

    cap = cv2.VideoCapture(0)
    while True:
        ret, _frame = cap.read()
        if not ret:
            break
        s_time = time.time()
        _faces = detect_face(_frame)
        if _faces:
            print(time.time() - s_time)
        for r in [f['rect'] for f in _faces]:
            cv2.rectangle(_frame, (r[0], r[1]), (r[2], r[3]), (0, 0, 255), 2)
        cv2.imshow("Faces", _frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
