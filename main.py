#! /usr/bin/env python3
import csv
import datetime
import json
import numpy
import cv2
import time
import os
import paho.mqtt.client as mqtt
import jwt
from google_iot import Device
# from face_detector import detect_face
# from face_detector_dlib import detect_face
# from face_detector_haar import detect_face

_cur_dir = os.path.dirname(os.path.realpath(__file__))


CSV_FILE = 'result.csv'

# DIMENSION_VIDEO = (320, 240)
DIMENSION_VIDEO = (640, 480)  # For HAAR Cascade

DIMENSION_GENDER_MODEL = (227, 227)
DIMENSION_FACE_MODEL = (300, 300)


def is_rpi():
    return 'arm' in os.uname()[4]


if is_rpi():
    from mvnc import mvncapi as mvnc

    # Initialize the NCS
    mvnc.global_set_option(mvnc.GlobalOption.RW_LOG_LEVEL, 2)

    # Initialize the age detector
    with open(os.path.join(_cur_dir, 'ncs', 'graph_face'), mode='rb') as f:
        blob_face = f.read()

    # Initialize the gender detector
    with open(os.path.join(_cur_dir, 'ncs', 'graph_gender'), mode='rb') as f:
        blob_gender = f.read()
    gender_list = ['Male', 'Female']

    # Initialize the NCS
    devices = mvnc.enumerate_devices()
    if not devices:
        print('No device found!')
        exit(-1)
    device = mvnc.Device(devices[0])
    device.open()

    graph_face = mvnc.Graph('GraphFace')
    input_fifo_face, output_fifo_face = graph_face.allocate_with_fifos(device, blob_face)


# Load the mean file
ilsvrc_mean_face = numpy.load(os.path.join(_cur_dir, 'ncs', 'face_mean.npy')).mean(1).mean(1)
ilsvrc_mean_age_gender = numpy.load(os.path.join(_cur_dir, 'ncs', 'age_gender_mean.npy')).mean(1).mean(1)


def _preprocess_image(image, _type='face'):
    ilsvrc_mean = ilsvrc_mean_face if _type == 'face' else ilsvrc_mean_age_gender
    image = cv2.resize(image, DIMENSION_GENDER_MODEL)
    image = image.astype(numpy.float32)
    image[:, :, 0] = (image[:, :, 0] - ilsvrc_mean[0])
    image[:, :, 1] = (image[:, :, 1] - ilsvrc_mean[1])
    image[:, :, 2] = (image[:, :, 2] - ilsvrc_mean[2])
    return image


def detect_face(frame):
    _frame = _preprocess_image(frame, _type='face')
    _resized = cv2.resize(_frame, DIMENSION_FACE_MODEL)
    graph_face.queue_inference_with_fifo_elem(input_fifo_face, output_fifo_face, _resized, 'Face Object')
    output, user_obj = output_fifo_face.read_elem()
    return output, user_obj


def detect_age_gender(frame, faces):
    largest_face = faces[0]['frame']
    _face = _preprocess_image(largest_face, _type='age_gender')

    # age = detect_age(_face)
    age = {'age': 'N/A', 'confidence': 0}  # Don't use age detector for now

    time.sleep(0.1)  # Add some delay

    graph_gender.LoadTensor(_face.astype(numpy.float16), 'user object')
    _output, _userobj = graph_gender.GetResult()
    _order = _output.argsort()
    _last = len(_order) - 1
    _predicted = int(_order[_last])
    # graph_gender.DeallocateGraph()
    # device.CloseDevice()
    gender = dict(gender=gender_list[_predicted], confidence=round(100.0 * _output[_predicted]))

    # Draw result
    for r in [_f['rect'] for _f in faces]:
        cv2.rectangle(frame, (r[0], r[1]), (r[2], r[3]), (0, 0, 255), 2)
        cv2.putText(frame, 'Age: {}, {}%'.format(age['age'], age['confidence']),
                    (r[0], r[1] - 30), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 0, 0), 2)
        cv2.putText(frame, 'Gender: {}, {}%'.format(gender['gender'], gender['confidence']),
                    (r[0], r[1] - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 0, 0), 2)
    return frame, age, gender


def write_result(age, gender):
    headers = ['Date', 'Age', 'Age_Confidence', 'Gender', 'Gender_Confidence']
    with open(CSV_FILE, 'a') as _f:
        writer = csv.DictWriter(_f, delimiter=',', lineterminator='\n', fieldnames=headers)
        if os.stat(CSV_FILE).st_size == 0:
            writer.writeheader()
        writer.writerow(dict(
            Date=datetime.datetime.now().isoformat(),
            Age=age['age'],
            Age_Confidence=age['confidence'],
            Gender=gender['gender'],
            Gender_Confidence=gender['confidence'],
        ))


def get_serial():
    # Extract serial from cpuinfo file
    cpuserial = "s0000000000000000"
    _f = open('/proc/cpuinfo', 'r')
    for line in _f:
        if line[0:6] == 'Serial':
            cpuserial = 's' + line[10:26]
    _f.close()
    return cpuserial


def create_jwt(project_id, private_key_file, algorithm):
    token = {
        # The time that the token was issued at
        'iat': datetime.datetime.utcnow(),
        # When this token expires. The device will be disconnected after the
        # token expires, and will have to reconnect.
        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60),
        # The audience field should always be set to the GCP project id.
        'aud': project_id
    }

    # Read the private key file.
    with open(private_key_file, 'r') as _f:
        private_key = _f.read()

    print('Creating JWT using {} from private key file {}'.format(algorithm, private_key_file))

    return jwt.encode(token, private_key, algorithm=algorithm)


def get_client(project_id, cloud_region, registry_id, device_id, private_key_file,
               algorithm, ca_certs, mqtt_bridge_hostname, mqtt_bridge_port, iot_device=Device()):
    client = mqtt.Client(
        client_id=('projects/{}/locations/{}/registries/{}/devices/{}'.format(project_id, cloud_region, registry_id,
                                                                              device_id)))

    client.username_pw_set(
        username='unused',
        password=create_jwt(
            project_id, private_key_file, algorithm))

    client.tls_set(ca_certs=ca_certs)

    client.on_connect = iot_device.on_connect
    client.on_publish = iot_device.on_publish
    client.on_disconnect = iot_device.on_disconnect
    client.on_message = iot_device.on_message
    client.on_log = iot_device.on_log
    client.connect(mqtt_bridge_hostname, mqtt_bridge_port)
    mqtt_config_topic = '/devices/{}/config'.format(device_id)

    client.subscribe(mqtt_config_topic, qos=1)
    return client


def main():
    # Capture the video frame from the webcam
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FPS, 5.0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640.0)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480.)
    cap.set(cv2.CAP_PROP_BRIGHTNESS, 0.7)
    cap.set(cv2.CAP_PROP_CONTRAST, 0.5)
    cap.set(cv2.CAP_PROP_GAIN, 0.5)

    project_id = 'toutaudio'
    cloud_region = 'us-central1'
    registry_id = 'RBPi'
    # device_id = 's00000000dadb085e'
    device_id = get_serial()
    private_key_file = '{}/private_key.pem'.format(_cur_dir)
    algorithm = 'RS256'
    mqtt_bridge_hostname = 'mqtt.googleapis.com'
    mqtt_bridge_port = 8883
    ca_certs = '{}/roots.pem'.format(_cur_dir)
    iot_device = Device()
    # iot_device.wait_for_connection(5)

    client = get_client(project_id, cloud_region, registry_id, device_id, private_key_file, algorithm, ca_certs,
                        mqtt_bridge_hostname, mqtt_bridge_port, iot_device)

    mqtt_event_topic = '/devices/{}/events'.format(device_id)

    jwt_iat = datetime.datetime.utcnow()
    jwt_exp_mins = 30

    face_lost_focus = False
    mqtt_event_sended = False
    face_in_focus = False
    focus_timer = False

    while True:
        client.loop()
        seconds_since_issue = (datetime.datetime.utcnow() - jwt_iat).seconds
        if seconds_since_issue > 60 * jwt_exp_mins:
            print('Refreshing token after {}s'.format(seconds_since_issue))
            jwt_iat = datetime.datetime.utcnow()
            client = get_client(project_id, cloud_region, registry_id, device_id, private_key_file, algorithm, ca_certs,
                                mqtt_bridge_hostname, mqtt_bridge_port)

        ret, _frame = cap.read()
        if not ret:
            break
        _frame = cv2.resize(_frame, DIMENSION_VIDEO)
        _faces = detect_face(_frame)
        if _faces:
            if not face_in_focus:
                focus_timer = datetime.datetime.now()
                face_in_focus = True

            _frame, age, gender = detect_age_gender(_frame, _faces)
            if focus_timer:
                time_delta = datetime.datetime.now() - focus_timer
                if time_delta.seconds >= 1 and not mqtt_event_sended:
                    gender['date'] = datetime.datetime.now().__str__()
                    gender['deviceID'] = device_id
                    client.publish(mqtt_event_topic, json.dumps(gender), qos=1)
                    print('Published {}'.format(datetime.datetime.now()))
                    mqtt_event_sended = True

        elif not _faces and face_in_focus and mqtt_event_sended:
            if not face_lost_focus:
                face_lost_focus = datetime.datetime.now()
            elif face_lost_focus:
                delta = datetime.datetime.now() - face_lost_focus
                if delta.seconds > 2:
                    print('Face lost')
                    face_in_focus = False
                    mqtt_event_sended = False
                    iot_device.stop_play()

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    print('Starting Age/Gender detector')
    # main()

    print(detect_face(cv2.imread('face.jpg')))

    input_fifo_face.destroy()
    output_fifo_face.destroy()
    graph_face.destroy()
    device.close()
    device.destroy()
