# coding: utf-8

# In[11]:

# load required libraries

import numpy as np
import cv2
import time
import keyboard

# path to a folder where caffe is installed
caffe_root = open("caffe.config", "r").read()
import sys

sys.path.insert(0, caffe_root + 'python')
import caffe

# In[12]:

# face detection model

detect_net = cv2.dnn.readNetFromCaffe("deploy.prototxt", "res10_300x300_ssd_iter_140000.caffemodel")

# In[13]:

# loading the mean image

mean_filename = './mean.binaryproto'
proto_data = open(mean_filename, "rb").read()
a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
mean = caffe.io.blobproto_to_array(a)[0]

# In[14]:

# age network
# won't be used in this example

age_net_pretrained = './age_net.caffemodel'
age_net_model_file = './deploy_age.prototxt'
age_net = caffe.Classifier(age_net_model_file, age_net_pretrained,
                           mean=mean,
                           channel_swap=(2, 1, 0),
                           raw_scale=255,
                           image_dims=(256, 256))

# In[15]:

# gender network

gender_net_pretrained = './gender_net.caffemodel'
gender_net_model_file = './deploy_gender.prototxt'
gender_net = caffe.Classifier(gender_net_model_file, gender_net_pretrained,
                              mean=mean,
                              channel_swap=(2, 1, 0),
                              raw_scale=255,
                              image_dims=(256, 256))

# In[16]:

# age and gender labels

age_list = ['(0, 2)', '(4, 6)', '(8, 12)', '(15, 20)', '(25, 32)', '(38, 43)', '(48, 53)', '(60, 100)']
gender_list = ['Male', 'Female']


# In[17]:

# function for formatted timestamps

def timestamp():
    now = time.time()
    gmtime = time.gmtime(now)
    milliseconds = '%03d' % int((now - int(now)) * 1000)
    return time.strftime("%a, %d %b %Y %H:%M:%S:", gmtime) + milliseconds + " UTC"


# In[19]:

# initialize the video stream
print("[INFO] starting video stream...")
vc = cv2.VideoCapture(0)

# open a file to write model predictions
output_file = open("output.txt", "w")

# loop over the frames from the video stream
while True:
    # grab the frame from the threaded video stream
    ret, frame = vc.read()

    output_text = timestamp()

    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    # pass the blob through the network and obtain the detections and
    # predictions
    detect_net.setInput(blob)
    detections = detect_net.forward()

    # loop over the detections
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < 0.5:
            continue

        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")

        # enlarge the bounding box by approximately 20%
        sx = max(0, int(1.2 * startX - 0.2 * endX))
        sy = max(0, int(1.2 * startY - 0.2 * endY))
        ex = min(w, int(1.2 * endX - 0.2 * startX))
        ey = min(h, int(1.2 * endY - 0.2 * startY))

        # slice a face from a frame
        face = frame[sy:ey, sx:ex]

        output_text += " Face " + str(i + 1) + " : " + str((startX, startY, endX, endY)) + " {:.2f}%".format(
            confidence * 100)

        # feed the face to the gender net
        gender_pred = gender_net.predict([cv2.resize(face, (256, 256))])
        gender_label = gender_list[gender_pred[0].argmax()]
        gender_confidence = gender_pred[0][gender_pred[0].argmax()]

        output_text += " " + gender_label + " {:.2f}%".format(gender_confidence * 100)

    # write info to file
    output_file.write(output_text + "\n")

    # exit the loop after keyboard is presssed
    try:  # used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('q'):  # if key 'q' is pressed
            print("[INFO] ending video stream...")
            break  # finishing the loop
        else:
            pass
    except:
        print("[INFO] ending video stream...")
        break  # if user pressed other than the given key the loop will break

# do a bit of cleanup
vc.release()
cv2.destroyAllWindows()
output_file.close()

# In[ ]:
