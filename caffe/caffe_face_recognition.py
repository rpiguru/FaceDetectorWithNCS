# coding: utf-8

# In[1]:

# load required libraries

import numpy as np
import cv2

# path to a folder where caffe is installed
caffe_root = open("caffe.config", "r").read()
import sys

sys.path.insert(0, caffe_root + 'python')
import caffe

# In[2]:

# face detection model

detect_net = cv2.dnn.readNetFromCaffe("deploy.prototxt", "res10_300x300_ssd_iter_140000.caffemodel")

# In[3]:

# loading the mean image

mean_filename = './mean.binaryproto'
proto_data = open(mean_filename, "rb").read()
a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
mean = caffe.io.blobproto_to_array(a)[0]

# In[4]:

# age network
# won't be used in this example

age_net_pretrained = './age_net.caffemodel'
age_net_model_file = './deploy_age.prototxt'
age_net = caffe.Classifier(age_net_model_file, age_net_pretrained,
                           mean=mean,
                           channel_swap=(2, 1, 0),
                           raw_scale=255,
                           image_dims=(256, 256))

# In[5]:

# gender network

gender_net_pretrained = './gender_net.caffemodel'
gender_net_model_file = './deploy_gender.prototxt'
gender_net = caffe.Classifier(gender_net_model_file, gender_net_pretrained,
                              mean=mean,
                              channel_swap=(2, 1, 0),
                              raw_scale=255,
                              image_dims=(256, 256))

# In[6]:

# age and gender labels

age_list = ['(0, 2)', '(4, 6)', '(8, 12)', '(15, 20)', '(25, 32)', '(38, 43)', '(48, 53)', '(60, 100)']
gender_list = ['Male', 'Female']

# In[8]:

# initialize the video stream
print("[INFO] starting video stream...")
vc = cv2.VideoCapture(0)

# simple counter for frames to feed into the gender net
counter = 0

# loop over the frames from the video stream
while True:
    # grab the frame from the threaded video stream
    ret, frame = vc.read()

    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    # pass the blob through the network and obtain the detections and
    # predictions
    detect_net.setInput(blob)
    detections = detect_net.forward()

    # loop over the detections
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < 0.5:
            continue

        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")

        # enlarge the bounding box by approximately 20%
        sx = max(0, int(1.2 * startX - 0.2 * endX))
        sy = max(0, int(1.2 * startY - 0.2 * endY))
        ex = min(w, int(1.2 * endX - 0.2 * startX))
        ey = min(h, int(1.2 * endY - 0.2 * startY))

        # slice a face from a frame
        face = frame[sy:ey, sx:ex]

        # feed to the gender net every 10th frame
        if counter % 10 == 0:
            gender_pred = gender_net.predict([cv2.resize(face, (256, 256))])
            gender_label = gender_list[gender_pred[0].argmax()]
            gender_confidence = gender_pred[0][gender_pred[0].argmax()]

        # draw the bounding box of the face along with the associated
        # probability
        text = "Face: " + "{:.2f}%".format(confidence * 100)
        try:
            gender_text = gender_label + ": " + "{:.2f}%".format(gender_confidence * 100)
        except:
            gender_text = "Unknown gender"
        gender_y = startY - 10 if startY - 10 > 10 else startY + 10
        y = gender_y - 15 if gender_y - 10 > 10 else gender_y + 15
        cv2.rectangle(frame, (startX, startY), (endX, endY),
                      (0, 0, 255), 2)
        # enlarged box
        # cv2.rectangle(frame, (sx, sy), (ex, ey),
        #     (255, 0, 0), 2)
        cv2.putText(frame, text, (startX, y),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
        cv2.putText(frame, gender_text, (startX, gender_y),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

    # add 1 to the counter
    counter += 1

    # show the output frame
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

# do a bit of cleanup
vc.release()
cv2.destroyAllWindows()

# In[ ]:
