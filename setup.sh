#!/bin/bash

sudo apt-get update
sudo apt-get install -y git

echo "Installing NCSDK..."
cd ~
git clone -b ncsdk2 https://github.com/movidius/ncsdk.git
cd ncsdk
make install

# Enable PiCamera
echo "start_x=1" | sudo tee -a /boot/config.txt

sudo apt-get install -y libffi-dev libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 ffmpeg libav-tools libssl-dev
sudo apt-get install -y python-pip
sudo pip install -U pip
sudo pip install -r requirements.txt

# Enable auto-start
sudo apt-get install screen
sudo sed -i -- "s/^exit 0/screen -mS face -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S face -X stuff \"cd \/home\/pi\/face-recognition-model\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S face -X stuff \"python main.py\\\\r\"\\nexit 0/g" /etc/rc.local

echo "Finished Installation, now rebooting!"
#sudo reboot
