import datetime
import json
import os
import time
from multiprocessing import Process
import paho.mqtt.client as mqtt
from playsound import playsound
import requests
from google.cloud import storage


class Device(object):
    def __init__(self):
        # self.connected_event = threading.Event()
        self.skipped_boot_config = False
        self.threads_list = []
        self._cur_dir = os.path.dirname(os.path.realpath(__file__))

    # def on_subscribe(self, unused_client, unused_userdata, unused_mid, granted_qos):
    #     """Callback when the device receives a SUBACK from the MQTT bridge."""
    #     if granted_qos[0] == 128:
    #         print('Subscription failed.')
    #     else:
    #         print('Subscribed: ', granted_qos)

    # def wait_for_connection(self, timeout):
    #     """Wait for the device to become connected."""
    #     # The wait() method takes an argument representing the
    #     # number of seconds to wait before the event times out.
    #     print('Device is connecting...')
    #     while not self.connected_event.wait(timeout):
    #         raise RuntimeError('Could not connect to MQTT bridge.')

    def error_str(self, rc):
        """Convert a Paho error to a human readable string."""
        return '{}: {}'.format(rc, mqtt.error_string(rc))

    def on_connect(self, unused_client, unused_userdata, unused_flags, rc):
        """Callback for when a device connects."""
        print('on_connect', mqtt.connack_string(rc))
        # if rc != 0:
        #     print('Error connecting:', self.error_str(rc))
        # else:
        #     print('Connected successfully.')
        # self.connected_event.set()

    def on_log(self, unused_client, unused_userdata, unused_flags, string):
        print("{} MQTT LOG: {}".format(datetime.datetime.now(), string))

    def on_disconnect(self, unused_client, unused_userdata, rc):
        """Paho callback for when a device disconnects."""
        print('Disconnected:', self.error_str(rc))
        # self.connected_event.clear()

    def on_publish(self, unused_client, unused_userdata, unused_mid):
        """Paho callback when a message is sent to the broker."""
        print('event published')

    def on_message(self, unused_client, unused_userdata, message):
        if not self.skipped_boot_config:
            self.skipped_boot_config = True
            return
        """Callback when the device receives a message on a subscription."""
        payload = message.payload.decode('utf-8')
        print('Received message \'{}\' on topic \'{}\' with Qos {}'.format(payload, message.topic, str(message.qos)))

        # The device will receive its latest config when it subscribes to the
        # config topic. If there is no configuration for the device, the device
        # will receive a config with an empty payload.
        if not payload:
            return

        data = json.loads(payload)
        media = data['media']
        token = data['confirmation_token']
        event_date = data['event_date']
        print('Received new config. Time: {}'.format(datetime.datetime.now()))
        bucket_name = media['bucket_name']
        print('Bucket name is: \'{}\''.format(bucket_name))
        config_name = media['gcs_file_name']
        print('Config name is: \'{}\''.format(config_name))
        # Destination file name is a byte literal because it's a file
        # name.
        destination_file_name = media['destination_file_name']

        self.download_blob(bucket_name, config_name, destination_file_name, event_date, token)

    def download_blob(self, bucket_name, config_name, destination_file_name, event_date, token):

        if not os.path.isfile(destination_file_name):
            """Downloads a blob from the bucket."""
            storage_client = storage.Client()
            bucket = storage_client.get_bucket(bucket_name)
            blob = bucket.blob(config_name)
            print('Download start at {}'.format(datetime.datetime.now()))
            blob.download_to_filename('{}/{}'.format(self._cur_dir, destination_file_name))
            print('Config {} downloaded to {}. Download time: {}'.format(
                config_name, destination_file_name, datetime.datetime.now()))
            # destination_file_name = 'https://storage.googleapis.com/toutaudio-media/{}'.format(config_name)
            destination_file_name = '{}/{}'.format(self._cur_dir, config_name)
        # destination_file_name = 'https://storage.googleapis.com/toutaudio-media/{}'.format(config_name)
        time_delta = datetime.datetime.now() - datetime.datetime.strptime(event_date, '%Y-%m-%d %H:%M:%S.%f')

        while time_delta.seconds < 2:
            time.sleep(0.1)
            time_delta = datetime.datetime.now() - datetime.datetime.strptime(event_date, '%Y-%m-%d %H:%M:%S.%f')

        headers = {'x-access-token': token,
                   'Authorization': 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhYzBjY2VlZWE5Y2VhM2FkNzhmY2FmOSIsImVtYWlsIjoiZXVnZW5peUBkYXRhc3RyYXRlZ3lsYWIuY29tIiwicm9sZSI6ImFkbWluIiwiY29tcGFueUlkIjoiNWFiYWI5NTcwZTg2NzJhNTY0MGZjMzRlIiwicGFzc3dvcmRIYXNoIjoiJDJhJDEwJDZzUlZUS1J3VDcwOTRFVFRIU1RmOS5IbDJuZ21MRVV1Y2VvUUNLdy9EeW5yYW1KYzBTMHZHIiwiaWF0IjoxNTI1NTA2Mzc5LCJleHAiOjQxMTc1MDYzNzl9.Xz03H_GD6vma4PWuXaaAPCl4mGBT2yja9LGyLtzuokA'}
        req = requests.get(url='https://api-develop-dot-toutaudio.appspot.com/v1/campaign/playback/confirmation',
                           headers=headers)
        res = req.json()
        print('Confirmation response: {}'.format(res))
        play_proc = Process(target=self.play_media, args=(destination_file_name,))
        play_proc.start()
        self.threads_list.append(play_proc)

    @staticmethod
    def play_media(file_name):
        playsound(file_name)
        # chunk = 1024
        # wf = wave.open(file_name, 'rb')
        # stream = pyaud.open(format=pyaud.get_format_from_width(wf.getsampwidth()), channels=wf.getnchannels(),
        #                     rate=wf.getframerate(), output=True)
        # data = wf.readframes(chunk)
        # while len(data) > 0:
        #     stream.write(data)
        #     data = wf.readframes(chunk)

    def stop_play(self):
        try:
            for thread in self.threads_list:
                try:
                    thread.terminate()
                except:
                    print('Thread termination problem')
        except AttributeError as er:
            pass
