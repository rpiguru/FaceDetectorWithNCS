import time
import dlib
import cv2


dlib_detector = dlib.get_frontal_face_detector()

extend_factor = .1

DIMENSION = (320, 240)


def detect_face(frame):
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    img_height, img_width, _ = frame.shape
    resized_frame = cv2.resize(gray_frame, DIMENSION)
    face_rects = dlib_detector(resized_frame, 1)
    sorted_rects = []
    if face_rects:
        face_rects = sorted(face_rects, key=lambda f: abs(f.left() - f.right()) * abs(f.top() - f.bottom()),
                            reverse=True)
        for r in face_rects:
            if any([v < 0 for v in [r.left(), r.right(), r.bottom(), r.top()]]):
                continue
            # Extend face rect.
            w = abs(r.right() - r.left())
            h = abs(r.top() - r.bottom())
            top = max(int((r.top() - h * extend_factor * 4) * img_height / DIMENSION[1]), 0)
            bottom = min(int(r.bottom() * img_height / DIMENSION[1]), img_height)
            left = max(int((r.left() - w * extend_factor) * img_width / DIMENSION[0]), 0)
            right = min(int((r.right() + w * extend_factor) * img_width / DIMENSION[0]), img_width)
            sorted_rects.append(dict(
                rect=[left, top, right, bottom],
                frame=frame[top:bottom, left:right]
            ))
    return sorted_rects


if __name__ == '__main__':

    cap = cv2.VideoCapture(0)
    while True:
        ret, _frame = cap.read()
        if not ret:
            break
        s_time = time.time()
        _faces = detect_face(_frame)
        if _faces:
            print('Elapsed: {}'.format(time.time() - s_time))
        for rr in [f['rect'] for f in _faces]:
            cv2.rectangle(_frame, (rr[0], rr[1]), (rr[2], rr[3]), (0, 0, 255), 2)
        cv2.imshow("Faces", _frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
