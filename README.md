# face-recognition-model
Face detection and recognition part of the AK-IoT-retail project.

## Installation

    cd ~
    git clone https://github.com/DataStrategyLab/face-recognition-model
    cd face-recognition-model
    bash setup.sh

